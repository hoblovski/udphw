#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include <sys/select.h>

#include "common.h"


#define MAX_CLI_RECVBUFSZ 16384
#define RECV_WITHIN_TIME_TIMEOUT 2

int recv_within_time(int fd, char* buf, size_t bufsz,
		struct sockaddr* addr, socklen_t *len,
		struct timeval* tv);

void do_request(int sockfd,
		struct sockaddr* srvaddr, socklen_t srvaddr_len,
		int number);


int main(int argc, char **argv)
{
	if (argc != 3 && argc != 4)
		Fatal("usage: client [SRVIP=localhost] SRVPORT NUMBER");

	// Create srvaddr
	const char* srvip;
	int srvport;
	int number;
	static const char* localhost_str = "127.0.0.1";
	if (argc == 3) {
		srvip = localhost_str;
		// atoi is less safe than strtol
		//	but here it's ok to use it
		srvport = atoi(argv[1]);
		number = atoi(argv[2]);
	} else {
		srvip = argv[1];
		srvport = atoi(argv[2]);
		number = atoi(argv[3]);
	}
	// Although we have to check for this again in the serverside
	if (number < 0)
		Fatal("NUMBER cannot be less than zero!");
	struct sockaddr_in srvaddr;
	sockaddr_create_ips(&srvaddr, srvip, srvport);

	// create sockfd
	int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd == -1)
		Fatal("clientmain: socket failed");

	// all the dirty stuff
	do_request(sockfd, (struct sockaddr*) &srvaddr, sizeof(srvaddr),
			number);

	exit(0);
}


int recv_within_time(int fd, char* buf, size_t bufsz,
		struct sockaddr* addr, socklen_t *len,
		struct timeval* tv)
{
	fd_set readfds;
	FD_ZERO(&readfds); FD_SET(fd,&readfds);

	if (select(fd+1, &readfds, NULL, NULL, tv) > 0) {
		if (FD_ISSET(fd, &readfds))
			return recvfrom(fd, buf, bufsz, 0, addr, len);
	}

	return -RECV_WITHIN_TIME_TIMEOUT;
}


void do_request(int sockfd,
		struct sockaddr* srvaddr, socklen_t srvaddr_len, int number)
{
	int number_nbo = htonl(number); // number in NETWORK BYTE ORDER

	sendto(sockfd, &number_nbo, sizeof(number_nbo), 0, srvaddr, srvaddr_len);

	static char recvbuf[MAX_CLI_RECVBUFSZ];
	// retransmission only happens if first recv timeouts
	int is_first_recv = 1;
	int n_recvtot = 0;
	while (1) {
		// block and receive a message
		struct sockaddr_in replyaddr;
		socklen_t replyaddr_len = sizeof(replyaddr);
		int n_recv = -1;
		while (1) {
			struct timeval tv = {1, 0}; // 1 secs
			n_recv = recv_within_time(sockfd, recvbuf, MAX_CLI_RECVBUFSZ,
					(struct sockaddr*) &replyaddr, &replyaddr_len,
					&tv);
			switch (n_recv) {
			case -RECV_WITHIN_TIME_TIMEOUT:
				if (is_first_recv) { 
					printf("[E] first recv timeout, retransmission...\n");
					sendto(sockfd, &number_nbo, sizeof(number_nbo),
							0, srvaddr, srvaddr_len);
				} else {
					printf("[E] intermediate recv timeout, waiting...\n");
					// don't make another request
					// because the server has already ack'ed our request
				}
				break;
			case -1:
				Fatal("[E] recv failed (errno=%d).\n", errno);
				break;
			default:
				// meaningful data received. send an acknowledgement
				is_first_recv = 0;
			}
			if (n_recv >= 0) break;
		}

		// ignore reply from hosts other than srvaddr
		if (replyaddr_len != srvaddr_len
				|| memcmp(srvaddr, &replyaddr, srvaddr_len)) {
			// replyaddr differ from srvaddr
			char replyaddr_str[INET_ADDRSTRLEN+1];
			if (inet_ntop(AF_INET, &(replyaddr.sin_addr),
						replyaddr_str, replyaddr_len) != NULL) {
				printf("[E] reply from %s (port=%d) ignored\n", replyaddr_str,
						replyaddr.sin_port);
			} else {
				printf("[E] inet_ntop failed. reply ignored.\n");
			}
		}

		// dump information
		printf("[I] received %d bytes from server:\n", n_recv);
		dump_mem(recvbuf, n_recv);
		n_recvtot += n_recv;

		// check for termination: terminate on receiving 0x00
		int has_strterm = 0;
		for (char* t = recvbuf; t < recvbuf + n_recv; t++)
			if (*t == 0) {
				has_strterm = 1;
				break;
			}
		if (has_strterm)
			break;
	}

	printf("[I] received a total of %d bytes from server.\n", n_recvtot);
}

