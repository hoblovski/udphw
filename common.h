#ifndef COMMON_H
#define COMMON_H

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>

#define bzero(a, l) memset(a, 0, l)

/*
 * iph: in host order
 */
void sockaddr_create(struct sockaddr_in* addr, int iph, short port);
void sockaddr_create_ips(struct sockaddr_in* addr, const char* ip, short port);

void putb_hex(char v);
void dump_mem(char* v, int size);

static inline void Fatal(const char* msg, ...) {
    int errsv = errno; // good habit
    va_list ap;
    va_start(ap, msg);
    vprintf(msg, ap);
    printf("\nfatal: errno=%d\n", errsv);
    va_end(ap);
    exit(-1);
}

static inline void FatalR(const char* msg, int ret) {
    puts(msg);
    exit(ret);
}

#endif // COMMON_H
