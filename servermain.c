#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/select.h>

#include "common.h"


/*
 * The server does not handle all kinds of timeout, transmission error etc.
 */


// UDP packet max size: a bit less than 65536
// Ethernet MTU: around 1500
// TODO: consider fragmentation or udp max packet size
#define MAX_SRV_SENDBUFSZ 1500
static char sendbuf[MAX_SRV_SENDBUFSZ];


void server_listen(int sockfd);


int main(int argc, char* argv[])
{
	if (argc != 2)
		Fatal("Usage: server PORT\n");
	int ck; // store error code returned by standard functions

	// create srvaddr
	// atoi can be used here.
	int srvport = atoi(argv[1]);
	struct sockaddr_in srvaddr;
	sockaddr_create(&srvaddr, INADDR_ANY, srvport);

	// create and bind sockfd
	int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	ck = bind(sockfd, (struct sockaddr*) &srvaddr, sizeof(srvaddr));
	if (ck == -1)
		Fatal("servermain: bind\n");
	else
		printf("[I] server started on 0.0.0.0:%d ...\n", srvport);

	memset(sendbuf, 'x', MAX_SRV_SENDBUFSZ);
	server_listen(sockfd); // never return
	Fatal("wtf? server_listen exits?!\n");
}


void server_listen(int sockfd)
{
	int number_nbo;

	while (1) {
		// receive some message
		struct sockaddr_in cliaddr;
		socklen_t cliaddr_len = sizeof(cliaddr);
		int n_recv = recvfrom(sockfd, &number_nbo, sizeof(number_nbo),
				0, (struct sockaddr*) &cliaddr, &cliaddr_len);
		if (n_recv == -1) {
			printf("[E] recvfrom returned -1 (errno=%d)\n", errno);
			continue;
		}

		// print information
		char cliaddr_str[INET_ADDRSTRLEN+1];
		if (inet_ntop(AF_INET, &(cliaddr.sin_addr), cliaddr_str, cliaddr_len)
				!= NULL) {
			// cliaddr_str[INET_ADDRSTRLEN] = 0; // additional zero-terminate
			printf("[I] received %d bytes from %s:\n", n_recv, cliaddr_str);
			dump_mem((char*) &number_nbo, n_recv);
			if (n_recv != sizeof(number_nbo)) {
				printf("[E] bad payload size, ignoring this request.\n");
				continue;
			}
		} else {
			printf("[E] inet_ntop failed (errno=%d), ignoring request.\n",
					errno);
			continue;
		}

		// checking number legibility
		int number = ntohl(number_nbo);
		if (number < 0) {
			printf("[E] bad number, ignore this request.\n");
			continue;
		}
		number += 1; // terminating 0
		printf("[I] sending response of %d bytes", number);

		// send response
		while (1) {
			if (number <= 0) {
				break;
			} else if (number < MAX_SRV_SENDBUFSZ) {
				sendbuf[number-1] = 0;
				int n_send = sendto(sockfd, sendbuf, number,
						0, (struct sockaddr*) &cliaddr, cliaddr_len);
				if (n_send == -1) {
					number = -1;
					break;
				}
				printf(".\n");
				sendbuf[number-1] = 'x';
				number -= n_send;
			} else {
				int n_send = sendto(sockfd, sendbuf, MAX_SRV_SENDBUFSZ,
						0, (struct sockaddr*) &cliaddr, cliaddr_len);
				if (n_send == -1) {
					number = -1;
					break;
				}
				printf(".");
				number -= n_send;

#ifdef FLOWCONTROL_BY_TIMEOUT
				// flow control: wait 10us for each packet 
				// It's of course stupid
				// It merely demonstrates the flow control problem.
				struct timeval tv = {0, 10};
				select(1, NULL, NULL, NULL, &tv);
#endif


			}
		}

		if (number == 0)
			printf("[I] response sent.\n");
		else
			printf("[E] error while sending response.\n");
		printf("\n");
	}
}
